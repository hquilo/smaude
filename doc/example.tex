\documentclass[fleqn]{elsarticle}

\usepackage{amsmath,amssymb,amsthm,fancyvrb,xcolor}
\usepackage[linesnumbered,lined,boxed]{algorithm2e}
\usepackage{cite,url,float,soul,alltt,multirow}

\newfloat{program}{thp}{lop}
\floatname{program}{Program}

% Additional macros
%\newenvironment{code}%
%{\begin{Verbatim}}%
%{\end{Verbatim}}

%environments
\newdefinition{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Theorem}
\newtheorem{example}{Example}
%\newproof{proof}{Proof}

\newcommand{\cesar}[1]{\textcolor{red}{[Cesar writes: #1]}}
\newcommand{\camilo}[1]{\textcolor{blue}{[Camilo writes: #1]}}
\newcommand{\mem}[2]{\textit{Mem}(#1,#2)}
\newcommand{\assg}[2]{#1{:=}#2}
\newcommand{\eval}{\evl}
\newcommand{\semrel}{\rightsquigarrow}
\newcommand{\serel}[2]{\semrel_{#1}^{#2}}
\newcommand{\nserel}[2]{\not\semrel_{#1}^{#2}}

\newcommand{\Univ}{\mathcal{U}}
\newcommand{\rel}{\rightarrow}
\newcommand{\Rel}{\Rightarrow}
\newcommand{\pair}[2]{\langle #1 \,; #2 \rangle}
\newcommand{\ppair}[2]{\langle #1 ; #2 \rangle}
\newcommand{\normrel}{\rel^\downarrow}
\newcommand{\arel}{\rel^\diamond}
\newcommand{\prel}{\rel^\Vert}
\newcommand{\abst}{T}
\newcommand{\paref}[1]{\stackrel{#1}{\rel_{\flat}}}
\newcommand{\parel}[1]{\stackrel{#1}{\rel_{\lcal}}}
\newcommand{\Parel}[1]{\stackrel{#1}{\rel}}

\newcommand{\ecal}{\mathcal{E}}
\newcommand{\pcal}{\mathcal{P}}
\newcommand{\kcal}{\mathcal{K}}
\newcommand{\lcal}{\mathcal{L}}
\newcommand{\tcal}{\mathcal{T}}
\newcommand{\rcal}{\mathcal{R}}
\newcommand{\acal}{\mathcal{A}}
\newcommand{\scal}{\mathcal{S}}
\newcommand{\ucal}{\mathcal{U}}
\newcommand{\ncal}{\mathcal{N}}
\newcommand{\strat}{\mathfrak{s}}
\newcommand{\state}{\textit{State}}
\newcommand{\Bool}{\mathbb{B}}
\newcommand{\plx}{\textrm{PLEXIL}}
\newcommand{\globally}{\square}
\newcommand{\finally}{\Diamond}
\newcommand{\next}{\bigcirc}
\newcommand{\until}{\mathbf{U}}
\newcommand{\wuntil}{\mathbf{W}}
\newcommand{\release}{\mathbf{R}}

\newcommand{\rews}{\Rightarrow}
\newcommand{\prews}{\rews^\|}
\newcommand{\srews}[1]{{\rews}^{#1}}
\newcommand{\nsrews}[1]{{\not\rews}^{#1}}
\newcommand{\Rews}{\Rightarrow}

\newcommand{\crl}[4]{#4 : #1 \Rel #2 \;\text{\bf if} \; #3}
\newcommand{\rl}[3]{#3 : #1 \Rel #2}
\newcommand{\seq}[2]{#1 \Rel #2}
\newcommand{\ceq}[3]{#1 = #2 \;\text{\bf if} \; #3}
\newcommand{\eq}[2]{#1 = #2}

\newcommand{\vars}[1]{\textit{vars}(#1)}
\newcommand{\obj}[2]{\langle #1 \mid #2 \rangle}
\newcommand{\tsort}[1]{[#1]}
\newcommand{\dom}[1]{\textit{dom}(#1)}
\newcommand{\ran}[1]{\textit{ran}(#1)}
\newcommand{\mclos}[1]{\overline{#1}}
\newcommand{\sort}[1]{\textit{#1}}
\newcommand{\algn}[1]{\text{\sf #1}}
\newcommand{\mmto}{\!:\!}
\newcommand{\evl}{\ccode{eval}}
\newcommand{\func}[3]{#1 : #2 \longrightarrow #3}
\newcommand{\ignore}[1]{}
\newcommand{\ccode}[1]{\textit{#1}}
%\newcommand{\ccode}[1]{\text{\sort{#1}}}
\newcommand{\rlname}[2]{#1\text{--}#2}
\newcommand{\Asg}{\leftarrow}

\newcommand{\true}{\textit{true}}
\newcommand{\false}{\textit{false}}
\newcommand{\bool}{\textit{Bool}}
% sorts
\newcommand{\Ctx}{\sort{Ctx}}

\DefineVerbatimEnvironment%
{code}{Verbatim}
{fontsize=\scriptsize,fontfamily=courier,fontshape=it,codes={\catcode`$=3}}

% Author information
%\title{An Example: Executable Semantics of a Simple Synchronous Language}

% Camilo
%\author[eci]{Camilo Rocha}
%\address[eci]{Escuela Colombiana de Ingenier\'{i}a, Bogot\'{a} D.C., Colombia}
%\ead{camilo.rocha@escuelaing.edu.co}
% César
%\author[nasa]{C\'esar Mu\~noz}
%\address[nasa]{NASA Langley Research Center, Hampton VA, USA}
%\ead{cesar.a.munoz@nasa.gov}

\begin{document}

This document illustrates the use of $\ccode{SMAUDE}$ by giving the
small-step semantics of a simple synchronous language with arithmetic
expressions. Code snippets of the Maude language are used to
illustrate explicitly how the infrastructure in \textit{SMAUDE} is
extended. Therefore, some familiarity with Maude's syntax is assumed
(see~\citep{maude-book} for a reference to the Maude language and
system).

Consider a language $\scal$ that consists of two kinds of elements:
{\em memory} elements $\mem{m}{v}$ and {\em assignment} elements
$\assg{m}{e}$, where $m$ denotes a memory name, $v$ denotes a
numerical value, and $e$ denotes an arithmetic expression.  Arithmetic
expressions are recursively formed using memory names, numerical
values, and expressions of the form $e_1 + e_2$, where $e_1$ and $e_2$
are arithmetic expressions. In this case, set $T$ consists of all
elements having the form $\mem{m}{v}$ or $\assg{m}{v}$.

The small-step semantics of $\scal$ requires the definition of an
evaluation function $\evl$ that takes as inputs a context $\Gamma$,
which is a set of elements $T$, and an arithmetic expression $e$. It
is inductively defined on expressions:

{\small
\[
\evl(\Gamma,e) = 
\begin{cases}
v & \text{if $e$ is the numerical value $v$,} \\
v & \text{if $e$ is the memory name $m$ and $\mem{m}{v}\in \Gamma$,} \\
v_1 + v_2 & \text{if $e$ has the form $e_1 + e_2$, $v_i = \evl(\Gamma,e_i)$ for $i\in\{1,2\}$}. \\
\end{cases}
\]
}

The (parametric) atomic relation $\rel_\scal$ of the language $\scal$
is defined for a context $\Gamma$ by $A \rel^\Gamma_\scal B$ if and
only if $A\subseteq \Gamma$, $A = \{\mem{m}{v},\assg{m}{e}\}$, $B =
\{\mem{m}{u},\assg{m}{e}\}$, and $u = \evl(A,e)$, for some memory name
$m$, values $v$ and $u$, and expression $e$.  The semantic relation of
the language is the relation $\rel_\scal^{\Gamma,s}$ (or
$\rel_\scal^s$), where $s$ is the $\prec$-maximal
$\rel^{\Gamma}$-strategy, $\Gamma$ is a ground context, and
$\prec_\scal$ is the empty priority.

\begin{example}
\label{ex.fsos}
If $\Gamma = \{\mem{x}{3},\mem{y}{4},\assg{x}{y},\assg{y}{x}\}$, then:
{\small
\begin{align*}
\mem{x}{3},\mem{y}{4},\assg{x}{y},\assg{y}{x}& \;\rel_\scal^{\Gamma,s}\;  \; \mem{x}{4},\mem{y}{3},\assg{x}{y},\assg{y}{x}.
\end{align*}
}
\end{example}

Language $\scal$ is specified by the Maude system module
$\ccode{SIMPLE}$, which includes system module $\ccode{SMAUDE}$, and
has the following syntax:
%
{\small
\begin{alltt}
mod SIMPLE is
  including SMAUDE .

  eq MODULE-NAME = 'SIMPLE .
  ...
endm
\end{alltt}}
%
Note that constant $\ccode{MODULE-NAME}$ is identified with the quoted
identifier representing the name of module $\ccode{SIMPLE}$.

Element identifiers include the following constructors with
sort $\sort{Eid}$:
%
{\small
\begin{alltt}
  op a    : Nat -> Eid [ctor] .
  ops x y : -> Eid [ctor] .
\end{alltt}}
%
Memory elements use constructors $\ccode{x}$ and $\ccode{y}$
for element identifiers, and assignment elements use constructors $\ccode{a}(\_)$ 
for element identifiers.

Attribute identifiers include the following constructors with
sort $\sort{Aid}$:
%
{\small
\begin{alltt}
  ops mem body to : -> Aid [ctor] .
\end{alltt}}
%
Memory elements have attribute $\ccode{mem}$ as their only attribute,
while assignment elements have attributes $\ccode{body}$ and
$\ccode{to}$ as their only attributes.
In the syntax of $\ccode{SIMPLE}$, memory element $\mem{x}{v}$ 
and an assignment element $\assg{x}{e}$ can be represented, respectively,
by elements
%
{\small
\begin{alltt}
  < x | mem : v >   < a(1) | body : e, to : x > .
\end{alltt}}

Built-in natural numbers are values of the language and addition
corresponds to the built-in one in Maude. These are
specified in $\ccode{SIMPLE}$ with the following subsort and
operation declarations:
%
{\small
\begin{alltt}
  subsort Nat < Val .
  op _+_ : Expr Expr -> Expr [ditto] .
\end{alltt}}

Expressions are evaluated equationally by following the definition of
$\ccode{eval}$:
%
{\small
\begin{alltt}
  var  C  : Ctx .    vars I J  : Eid .  vars E E' : Expr .
  var  M  : Map .    var  N    : Nat .

  eq eval(C,N) = N .
  eq eval(( < I | mem : N , M > C), I ) = N .
  eq eval(C,E + E') = eval(C,E) + eval(C,E') .
\end{alltt}}

Atomic rule $\rlname{r}{1}$ specifies the 
atomic relation of the language:
%
{\small
\begin{alltt}
  rl [r-1] : 
     < I | mem : N > < J | body : E, to : I > 
     =>  < I | mem : eval(E) > .
\end{alltt}}
%
The specification of atomic rules is slightly different to the usual
specification of rules in rewriting logic. First, in the lefthand side
of an atomic rule, it is sufficient to only mention the attributes
involved in the atomic transition. In this case, $\ccode{SMAUDE}$ will
complete each lefthand side term by automatically adding a variable of
sort $\sort{Map}$, unique for each element, before any matching is
performed.  Second, in the righthand side of an atomic rule, it is
sufficient to only mention the elements and the attributes that can
change in the atomic step. In this case, $\ccode{SMAUDE}$ updates in
the current state {\em only} the attributes of the elements occurring
in the righthand side of the rule, while keeping the other ones
intact.  So, in atomic rule $\rlname{r}{1}$, the only attribute that
can change is attribute $\ccode{mem}$ of the memory element. Note also
that in the righthand side of $\rlname{r}{1}$, a unary version of
function $\ccode{eval}$, without mention of any particular context, is
used; $\ccode{SMAUDE}$ will automatically extend it to its binary
counterpart, for the given context, when computing function
$\ccode{max-strat}$.

The context $\Gamma$ in Example~\ref{ex.fsos}, written in the
syntax of $\ccode{SIMPLE}$, is
%
{\small
\begin{alltt}
  < x | mem : 3 >  < y | mem : 4 >  
  < a(1) | body : y, to : x >  < a(2) | body : x, to : y > .
\end{alltt}}
%
Maude's $\ccode{search}$ command can be used to compute,
for instance, the one-step synchronous semantic relation of the 
language in Example~\ref{ex.fsos} from context $\Gamma$:

{\small
\begin{alltt}
Maude> search \{ \(\Gamma\) \} =>1 X:Sys .
search in SIMPLE : \{ \(\Gamma\) \} =>1 X:Sys .
Solution 1 (state 1)
states: 2  rewrites: 514 in 53ms cpu (54ms real) (9655 rewrites/second)
X:Sys --> \{< x | mem : 4 > < y | mem : 3 > 
            < a(1) | body : y, to : x > < a(2) | body : x, to : y > \}
No more solutions.
\end{alltt}}
\begin{thebibliography}{10}

\bibitem{maude-book}
M.~Clavel, F.~Dur{\'a}n, S.~Eker, P.~Lincoln, N.~Mart\'{\i}-Oliet, J.~Meseguer,
  and C.~L. Talcott, editors.
\newblock {\em All About Maude - A High-Performance Logical Framework, How to
  Specify, Program and Verify Systems in Rewriting Logic}, volume 4350 of {\em
  Lecture Notes in Computer Science}.
\newblock Springer, 2007.

\end{thebibliography}


\end{document}
